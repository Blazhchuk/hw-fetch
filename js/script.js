

const filmsDiv = document.getElementById('films');

function fetchFilms(film) {
    return Promise.all(film.characters.map(url =>
        fetch(url)
            .then(response => response.json())
    ));
}

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => {
        films.forEach(film => {
            const filmElement = document.createElement('div');
            filmElement.innerHTML = `<h2>Episode ${film.episodeId}: ${film.name}<h2><p>${film.openingCrawl}</p>`;
            filmsDiv.appendChild(filmElement);

            const loadingElement = document.createElement('div');
            loadingElement.classList.add('loading');
            filmElement.appendChild(loadingElement);

            fetchFilms(film)
                .then(characters => {
                    loadingElement.remove();
                    const charactersList = document.createElement('ul');
                    characters.forEach(characters => {
                        const characterItem = document.createElement('li');
                        characterItem.textContent = characters.name;
                        charactersList.appendChild(characterItem);
                    });
                    filmElement.appendChild(charactersList);
                })
        })
    })
